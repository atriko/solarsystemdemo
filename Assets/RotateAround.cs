﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    public GameObject objectToRotateAround;
    public float rotateSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(objectToRotateAround.transform.position, Vector3.up, rotateSpeed * Time.deltaTime);
    }
}
